EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Adapter MS88SF2"
Date "2020-05-10"
Rev "0.1"
Comp "Matthias Deimbacher"
Comment1 "CERN Open Hardware Licence Version 2 - Strongly Reciprocal"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L my_microcontroller:MS88SF2 U1
U 1 1 5EB9BFB5
P 3450 3900
F 0 "U1" H 3450 5115 50  0000 C CNN
F 1 "MS88SF2" H 3450 5024 50  0000 C CNN
F 2 "my_microcontroller:MS88SF2" H 3450 2700 50  0001 C CNN
F 3 "https://infocenter.nordicsemi.com/pdf/nRF52840_PS_v1.1.pdf" H 3500 2600 50  0001 C CNN
	1    3450 3900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x14 J1
U 1 1 5EB9DFBD
P 7050 3850
F 0 "J1" H 6968 4667 50  0000 C CNN
F 1 "Conn_01x14" H 6968 4576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x14_P2.54mm_Vertical" H 7050 3850 50  0001 C CNN
F 3 "~" H 7050 3850 50  0001 C CNN
	1    7050 3850
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x14 J2
U 1 1 5EB9EA43
P 9400 3950
F 0 "J2" H 9318 3025 50  0000 C CNN
F 1 "Conn_01x14" H 9318 3116 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x14_P2.54mm_Vertical" H 9400 3950 50  0001 C CNN
F 3 "~" H 9400 3950 50  0001 C CNN
	1    9400 3950
	1    0    0    1   
$EndComp
Text GLabel 7750 3250 2    50   Output ~ 0
P1
Text GLabel 7750 3350 2    50   BiDi ~ 0
P2
Text GLabel 7750 3450 2    50   BiDi ~ 0
P3
Text GLabel 7750 3550 2    50   BiDi ~ 0
P4
Text GLabel 7750 3650 2    50   BiDi ~ 0
P5
Text GLabel 7750 3750 2    50   BiDi ~ 0
P6
Text GLabel 7750 3850 2    50   BiDi ~ 0
P7
Text GLabel 7750 3950 2    50   BiDi ~ 0
P8
Text GLabel 7750 4050 2    50   BiDi ~ 0
P9
Text GLabel 7750 4150 2    50   BiDi ~ 0
P10
Text GLabel 7750 4250 2    50   BiDi ~ 0
P11
Text GLabel 7750 4350 2    50   BiDi ~ 0
P12
Text GLabel 7750 4450 2    50   Output ~ 0
P13
Text GLabel 7750 4550 2    50   Output ~ 0
P14
Text GLabel 8800 4550 0    50   Output ~ 0
P15
Text GLabel 8800 4450 0    50   BiDi ~ 0
P16
Text GLabel 8800 4350 0    50   BiDi ~ 0
P17
Text GLabel 8800 4250 0    50   BiDi ~ 0
P18
Text GLabel 8800 4150 0    50   BiDi ~ 0
P19
Text GLabel 8800 4050 0    50   BiDi ~ 0
P20
Text GLabel 8800 3950 0    50   BiDi ~ 0
P21
Text GLabel 8800 3850 0    50   BiDi ~ 0
P22
Text GLabel 8800 3750 0    50   BiDi ~ 0
P23
Text GLabel 8800 3650 0    50   BiDi ~ 0
P24
Text GLabel 8800 3550 0    50   BiDi ~ 0
P25
Text GLabel 8800 3450 0    50   Output ~ 0
P26
Text GLabel 8800 3350 0    50   BiDi ~ 0
P27
Text GLabel 8800 3250 0    50   BiDi ~ 0
P28
Wire Wire Line
	7250 3250 7750 3250
Wire Wire Line
	7250 3350 7750 3350
Wire Wire Line
	7250 3450 7750 3450
Wire Wire Line
	7250 3550 7750 3550
Wire Wire Line
	7750 3650 7250 3650
Wire Wire Line
	7250 3750 7750 3750
Wire Wire Line
	7750 3850 7250 3850
Wire Wire Line
	7250 3950 7750 3950
Wire Wire Line
	7750 4050 7250 4050
Wire Wire Line
	7250 4150 7750 4150
Wire Wire Line
	7250 4250 7750 4250
Wire Wire Line
	7750 4350 7250 4350
Wire Wire Line
	7250 4450 7750 4450
Wire Wire Line
	7750 4550 7250 4550
Wire Wire Line
	8800 3250 9200 3250
Wire Wire Line
	9200 3350 8800 3350
Wire Wire Line
	8800 3450 9200 3450
Wire Wire Line
	9200 3550 8800 3550
Wire Wire Line
	8800 3650 9200 3650
Wire Wire Line
	9200 3750 8800 3750
Wire Wire Line
	8800 3850 9200 3850
Wire Wire Line
	9200 3950 8800 3950
Wire Wire Line
	8800 4050 9200 4050
Wire Wire Line
	9200 4150 8800 4150
Wire Wire Line
	8800 4250 9200 4250
Wire Wire Line
	9200 4350 8800 4350
Wire Wire Line
	8800 4450 9200 4450
Wire Wire Line
	9200 4550 8800 4550
Text GLabel 2350 4900 0    50   Input ~ 0
P1
Text GLabel 4500 3700 2    50   BiDi ~ 0
P2
Text GLabel 4500 4900 2    50   Input ~ 0
P13
Text GLabel 4500 2950 2    50   Input ~ 0
P14
Text GLabel 4500 4150 2    50   Input ~ 0
P15
Text GLabel 4500 4700 2    50   Input ~ 0
P26
Text GLabel 4500 3200 2    50   BiDi ~ 0
P20
Text GLabel 4500 3500 2    50   BiDi ~ 0
P24
Text GLabel 4500 3600 2    50   BiDi ~ 0
P11
Text GLabel 4500 3800 2    50   BiDi ~ 0
P3
Text GLabel 4500 4250 2    50   BiDi ~ 0
P17
Text GLabel 4500 4350 2    50   BiDi ~ 0
P16
Text GLabel 2350 3000 0    50   BiDi ~ 0
P4
Text GLabel 2350 3100 0    50   BiDi ~ 0
P8
Text GLabel 2350 3200 0    50   BiDi ~ 0
P9
Text GLabel 2350 3300 0    50   BiDi ~ 0
P10
Text GLabel 2350 3400 0    50   BiDi ~ 0
P27
Text GLabel 2350 3500 0    50   BiDi ~ 0
P28
Text GLabel 2350 3750 0    50   BiDi ~ 0
P12
Text GLabel 2350 3850 0    50   BiDi ~ 0
P18
Text GLabel 2350 3950 0    50   BiDi ~ 0
P19
Text GLabel 2350 4150 0    50   BiDi ~ 0
P21
Text GLabel 2350 4250 0    50   BiDi ~ 0
P22
Text GLabel 2350 4350 0    50   BiDi ~ 0
P23
Text GLabel 2350 4450 0    50   BiDi ~ 0
P7
Text GLabel 2350 4550 0    50   BiDi ~ 0
P5
Text GLabel 2350 4650 0    50   BiDi ~ 0
P6
Text GLabel 4500 4600 2    50   BiDi ~ 0
P25
Wire Wire Line
	4500 2950 4400 2950
Wire Wire Line
	4500 3200 4150 3200
Wire Wire Line
	4500 3500 4150 3500
Wire Wire Line
	4150 3600 4500 3600
Wire Wire Line
	4500 3700 4150 3700
Wire Wire Line
	4150 3800 4500 3800
Wire Wire Line
	4500 4150 4400 4150
Wire Wire Line
	4150 4250 4500 4250
Wire Wire Line
	4500 4350 4150 4350
Wire Wire Line
	4150 4600 4500 4600
Wire Wire Line
	4500 4700 4150 4700
Wire Wire Line
	4150 4900 4400 4900
Wire Wire Line
	2750 4900 2650 4900
Wire Wire Line
	2350 4650 2750 4650
Wire Wire Line
	2750 4550 2350 4550
Wire Wire Line
	2350 4450 2750 4450
Wire Wire Line
	2750 4350 2350 4350
Wire Wire Line
	2350 4250 2750 4250
Wire Wire Line
	2750 4150 2350 4150
Wire Wire Line
	2750 3950 2350 3950
Wire Wire Line
	2350 3850 2750 3850
Wire Wire Line
	2750 3750 2350 3750
Wire Wire Line
	2350 3500 2750 3500
Wire Wire Line
	2750 3400 2350 3400
Wire Wire Line
	2750 3300 2350 3300
Wire Wire Line
	2350 3200 2750 3200
Wire Wire Line
	2750 3100 2350 3100
Wire Wire Line
	2350 3000 2750 3000
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5EBF1FCE
P 4400 2850
F 0 "#FLG0101" H 4400 2925 50  0001 C CNN
F 1 "PWR_FLAG" H 4400 3023 50  0000 C CNN
F 2 "" H 4400 2850 50  0001 C CNN
F 3 "~" H 4400 2850 50  0001 C CNN
	1    4400 2850
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5EBF39AB
P 4400 4100
F 0 "#FLG0102" H 4400 4175 50  0001 C CNN
F 1 "PWR_FLAG" H 4400 4273 50  0000 C CNN
F 2 "" H 4400 4100 50  0001 C CNN
F 3 "~" H 4400 4100 50  0001 C CNN
	1    4400 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 4100 4400 4150
Connection ~ 4400 4150
Wire Wire Line
	4400 4150 4150 4150
Wire Wire Line
	4400 2850 4400 2950
Connection ~ 4400 2950
Wire Wire Line
	4400 2950 4150 2950
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5EBFBD00
P 4400 5050
F 0 "#FLG0103" H 4400 5125 50  0001 C CNN
F 1 "PWR_FLAG" H 4400 5223 50  0000 C CNN
F 2 "" H 4400 5050 50  0001 C CNN
F 3 "~" H 4400 5050 50  0001 C CNN
	1    4400 5050
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 5EBFC02B
P 2650 5050
F 0 "#FLG0104" H 2650 5125 50  0001 C CNN
F 1 "PWR_FLAG" H 2650 5223 50  0000 C CNN
F 2 "" H 2650 5050 50  0001 C CNN
F 3 "~" H 2650 5050 50  0001 C CNN
	1    2650 5050
	-1   0    0    1   
$EndComp
Wire Wire Line
	2650 5050 2650 4900
Connection ~ 2650 4900
Wire Wire Line
	2650 4900 2350 4900
Wire Wire Line
	4400 5050 4400 4900
Connection ~ 4400 4900
Wire Wire Line
	4400 4900 4500 4900
$EndSCHEMATC
